package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"https://logivice.net",
		"https://ynet.co.il",
		"https://googlmhe.com",
		"https://facebook.com",
	}
	channel := make(chan bool)
	for _, l := range links {
		go checkLink(l, channel)
		
		if <-channel {
			fmt.Println(l, " is OK")
		} else {
			fmt.Println(l, " is Dead")
		}
		
	}
}

func checkLink(l string, c chan bool) bool {
	time.Sleep(time.Second * 3)
	site, err := http.Get(l)
	if err != nil {
		c <- false
		return false
	}
	if site.StatusCode == 200 {
		c <- true
		return true
	}
	c <- false
	return false
}
